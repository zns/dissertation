# A History of Protein Folding

Gerardus Johannes Mulder (27 December 1802 – 18 April 1880) was a Dutch organic and analytical chemist. He was interested in characterizing biological compounds into their specific elements. Mulder was able to collect some proteins using a crude method. E.g. the protocol for purifying mysoin from frog muscles was to inject frogs with 1% salt solution, freeze, pound to a snow, and thaw and filter through a linen cloth (Willie Kuhne 1859). Though primitive, these early chemists were able to distinguish proteins and identify that their principal components were Carbon, Oxygen, Nitrogen, and Hydrogen. Mulder especially noted that different proteins had very similar relative abundances of each element, leading him to believe that they were an identical substance, which he called "Grundstoff." In Mulder's letters to Baron Jöns Jacob Berzelius (Swedish: [jœns ˌjɑːkɔb bæɹˈseːliɵs]; 20 August 1779 – 7 August 1848), Berzelius suggested using the name "protein" which comes from the greek word, πρωτεŧοζ, which means "standing in front" which seemed justified seeing that this sbustance might be the most important and unilateral substance in the composition of animal and plant matter.

The universality of the protein component was discarded after crystal formation was discovered, as very different crystals were reported for the proteins from different species. Crystallization helped immensely with purification and helped to herald the next scientists to be able to discover the macromolecular properties of proteins and how they are arranged. The discovery that proteins were composed of linked amino acids was discovered independently by two scientists. Hermann Emil Louis Fischer (9 October 1852 – 15 July 1919), who won the 1902 Nobel Prize in Purine Synthesis demonstrated that proteins were actually linked together by "peptide" bonds, and they were likely "polypeptide chains" (both terms that Fischer coined). Fischer had the ambitious goal of synthesizing a synthetic protein to be used to useful benefits, a goal that is still aspired to today by many scientists.  Fischer had a contemporary, Franz Hofmeister (30 August 1850, Prague – 26 July 1922, Würzburg), who also deducted that proteins were composed of "peptide bonds" by noting that C-C, ether and ester bonds were unlikely candidates, considering they can't be digested by trypsin and R=C-N-C=R bonds could be eliminated because it would imply a much larger number of carboxylate groups than is observed experimentally. Hofmesiter also developed the salt purification methods for protein purification, still used today and had the extraordinary prescience to predict that every reaction in the cell was due to a specific kind of protein. Together, and independently, Hofmeister and Fischer presented results on the peptide bond in the 74th meeting of the Gesellschaft deutscher Naturforscher und Ärzte in Karlsbad, (Czech Republic)  in 1902, which is considered to be the most important even in the history of protein science.

Still, though there was much enterprising science, the scientific community still argued about the existence of peptide bonds, arguing for "cyclol" theories. Some also argued that the macromolecular (molecules > 12000 daltons) did not exist, and that proteins themselves were collodial, and not enyzymatic except that they can carry some enzyme that is the true catalyst. But bigger questions still loomed - what was the structure of proteins? Are proteins enzymes? What kind of charges are on proteins? How are proteins the carriers of genetic information?
  REMOVE??

As more proteins were purified and discovered, a taxonomy of proteins was determined which still exists today. This taxonomy was defined by the solubility \cite{nomenclature1908}: "albumins" were defined as soluble in pure water; "globulins" defined as insoluble in water, but soluble in neutral salt conditions; "glutelins" defined as being soluble in  dilute acid or base; and "histones" were very soluble in water but mainly composed of basic amino acids and insoluble in dilute ammonia. All these proteins were also defined by the coagulability in heat.


After proteins were defined, the investigations on their properties began. Sir William Bate Hardy, (6 April 1864 – 23 January 1934) was able to show that proteins could move opposite directions in an electric field, depending on the pH. This was pivotal in establishing that proteins had surface charge and that they had a specific isoelectric point at which point the motion changed direction in a electric field. William Thomas Astbury (also Bill Astbury; 25 February 1898, Longton – 4 June 1961, Leeds) led early studies of X-ray diffraction on fibrous proteins. He proposed that proteins were likely stabilized by hydrogen bonds, and recognized the protein folding problem, saying that 

> This leaves us with the paradox that the pepsin molecule is both globular and also a real, or potential, polypeptide chain system. William Astbury, 1934

Around the same time, in 1926, it was shown by James B. Sumner that urease, an enzyme from jack bean plant, can be crystallized and is a protein (which won the Nobel Prize in 1946). This was a controversial question, because previously scientists had held that enzymatic reactions only carried in the presence of whole cells (though there had been evidence of cell lysates being able to ferment and digest proteins).


Influenced by Astbury and under the direction of Sir William Bragg, John Desmond Bernal  10 May 1901 – 15 September 1971) took the first sharp X-ray diffractions of protein crystals of pepsin in 1934. He helped to confirm that proteins were globular, and that there was a great deal of water in the spaces between molecules. Bernal's student, Max Perutz, would later determine the X-ray structure of hemomglobin to earn the Nobel Prize in 1962 with John Kendrew.
 The 3D structure of myoglobin by John Kendrew in 1958 provided the first actual model of a protein \cite{kendrew1958three}, which allowed for unprecedented insight. As Kendrew said:

> Perhaps the most remarkable features of the molecule are its complexity and its lack of symmetry. The arrangement seems to be almost totally lacking in the kind of regularities which one instinctively anticipates, and it is more complicated than has been predicted by any theory of protein structure.

At the same time structures were being investigated, the nature of protein folding was beginning to be investigated. As early as 1925, Mortimer (Tim) Louis Anson (1901 – 16 October 1968) and Alfred Ezra Mirsky (October 17, 1900 – June 19, 1974)  discovered that haemoglobin can be denatured and then renatured to combine with oxygen or to be crystallized. \cite{anson1931reversibility} This discovery was monumental, as it implied that protein denaturation is an equilibrium process and the transition between denaturation and native state was thermodynamic. Anson and Mirsky went on to postulate the driving forces saying that "The polypeptide chain is folded into a uniquely defined configuration, in which it is held by hydrogen bonds." Soon others weighed in as well, Bernal in 1939 declared that ionic bonds could not be the structural driving force as they would certainly hydrate, and he advocated the hydrophobic principle \cite{bernal1939structure} which was first proposed (in the protein context) by Irving Langmuir (January 31, 1881 – August 16, 1957) who had won the Nobel Prize for investigating hydrophobic effects in surface chemistry and thought these same principles applied to proteins \cite{langmuir1938protein}. Interestingly, this idea would lose traction until it was thoroughly defended by Walter Kauzmann in a review about protein denaturation \cite{kauzmann1954denaturation}.

Another boon in Protein research was the discovery of the DNA structure and its relation to genetic information. Previous to this, the protein was a popular molecule for transmitting genetic information. Even Linux Pauling held that proteins, which had varieties of forms and functions and variability, could easily account for heredity while DNA seemed "dumb" in comparision. After DNA was discovered, these "protein template" theories were discarded.

Once the tools were in place and the major breakthroughs had been made, the progress of protein science increased steadily. Once DNA was discovered as the genetic information, protein chemists sought and discovered the relationship between codons and amino acids quite quickly.  The codons were discovered in 1961, and within 5 years the 20 amino acids were deciphered \cite{frisch1966genetic}. Also in 1961, Christian Anfinsen discovered the enzymatic reversibility of RNase which earned him the Nobel Prize in 1972 - further solidifying the observations by Anson and Mirsky that protein folding is a equilibrium process and providing the thermodynamic hypothesis that the structure of a protein is uniquely encoded in the primary sequence.

> Prolongation of the physical life – with immortality as the ultimate goal—was the aim of the first phase of alchemy. O. B. Johnson, 1928

In the 1970's the proteins from cell membranes were begun to be investigated. New methods constantly emerged to study protein structure and folding, and new theories formed about their.



@inproceedings{frisch1966genetic,
  title={THE GENETIC CODE. Cold Spring Harbor Symposia on Quantitative Biology, Cold Spring Harbor, NY, June 2--9, 1966.},
  author={Frisch, L},
  booktitle={Cold Spring Harbor Symp. Quant. Biol., 31: 1-762 (1966).},
  number={CONF-660653},
  year={1966}
}

@article{pauling1953proposed,
  title={A proposed structure for the nucleic acids},
  author={Pauling, Linus and Corey, Robert B},
  journal={Proceedings of the National Academy of Sciences of the United States of America},
  volume={39},
  number={2},
  pages={84},
  year={1953},
  publisher={National Academy of Sciences}
}

@article{nomenclature1908,
  title={Recommendations of the committe on protein nomenclature},
  journal={American Journal of Physiology},
  volume={21},
  pages={xxvii-xxx},
  year={1908},
}

@article{kendrew1958three,
  title={A three-dimensional model of the myoglobin molecule obtained by x-ray analysis},
  author={Kendrew, John C and Bodo, G and Dintzis, Howard M and Parrish, RG and Wyckoff, Harold and Phillips, David C},
  journal={Nature},
  volume={181},
  number={4610},
  pages={662--666},
  year={1958}
}

@book{kauzmann1954denaturation,
  title={Denaturation of proteins and enzymes},
  author={Kauzmann, Walter},
  year={1954},
  publisher={Johns Hopkins Press: Baltimore}
}

@inproceedings{langmuir1938protein,
  title={Protein monolayers},
  author={Langmuir, Irving},
  booktitle={Cold Spring Harbor Symposia on Quantitative Biology},
  volume={6},
  pages={171--189},
  year={1938},
  organization={Cold Spring Harbor Laboratory Press}
}

@article{anson1931reversibility,
  title={The reversibility of protein coagulation},
  author={Anson, ML and Mirsky, AE},
  journal={The Journal of Physical Chemistry},
  volume={35},
  number={1},
  pages={185--193},
  year={1931},
  publisher={ACS Publications}
}

@article{bernal1939structure,
  title={Structure of proteins},
  author={Bernal, John D},
  journal={Journal of the Society of Dyers and Colourists},
  volume={55},
  number={6},
  pages={308--308},
  year={1939},
  publisher={Wiley Online Library}
}