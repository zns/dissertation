\documentclass[twoside,bind]{../dukedissertation}
% Use the second for a single-spaced copy suitable for duplex printing
% and binding.

% Other useful options (there are more options documented in Chapter 2):
%  * draft -- don't actually include images, print a black bar on overful
%             hboxes
%  * MS    -- Format for a Master's Thesis.  No UMI abstract page, some 
%             textual changes to title page.  


% Useful packages for dissertation writing:
\usepackage{amsmath, amssymb, amsfonts, amsthm}
\usepackage{graphicx}
\usepackage{natbib}
\usepackage{color}
\usepackage{bm}
\usepackage{subfigure}
\usepackage{graphicx}
\usepackage{mathabx}
\usepackage{multirow}
\usepackage{setspace}
\usepackage{fancyref}
\usepackage{microtype}
\usepackage{units}
\usepackage{siunitx}
%\usepackage{cite}  % If you include this, hyperlink cites will
                     % break.  It's nice to use this package if your bibstyle
							% sorts entries by order-of-use, rather than
							% alphabetically (as plain does).
							
%Theorem, Lemma, etc. environments
\newtheorem{theorem}{Theorem}%[section]
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{result}[theorem]{Result}

\def\mean#1{\left< #1 \right>}
\newcommand{\norm}[1]{\left\lvert#1\right\rvert}

% Personal commands and abbreviations.
\renewcommand{\labelenumi}{(\alph{enumi})} % Use letters for enumerate
% \DeclareMathOperator{\Sample}{Sample}
\let\vaccent=\v % rename builtin command \v{} to \vaccent{}
\renewcommand{\v}[1]{\ensuremath{\mathbf{#1}}} % for vectors
\newcommand{\gv}[1]{\ensuremath{\mbox{\boldmath$ #1 $}}} 
% for vectors of Greek letters
\newcommand{\uv}[1]{\ensuremath{\mathbf{\hat{#1}}}} % for unit vector
\newcommand{\abs}[1]{\left| #1 \right|} % for absolute value
\newcommand{\avg}[1]{\left< #1 \right>} % for average
\let\underdot=\d % rename builtin command \d{} to \underdot{}
\renewcommand{\d}[2]{\frac{d #1}{d #2}} % for derivatives
\newcommand{\dd}[2]{\frac{d^2 #1}{d #2^2}} % for double derivatives
\newcommand{\pd}[2]{\frac{\partial #1}{\partial #2}} 
% for partial derivatives
\newcommand{\pdd}[2]{\frac{\partial^2 #1}{\partial #2^2}} 
% for double partial derivatives
\newcommand{\pdc}[3]{\left( \frac{\partial #1}{\partial #2}
 \right)_{#3}} % for thermodynamic partial derivatives
\newcommand{\ket}[1]{\left| #1 \right>} % for Dirac bras
\newcommand{\bra}[1]{\left< #1 \right|} % for Dirac kets
\newcommand{\braket}[2]{\left< #1 \vphantom{#2} \right|
 \left. #2 \vphantom{#1} \right>} % for Dirac brackets
\newcommand{\matrixel}[3]{\left< #1 \vphantom{#2#3} \right|
 #2 \left| #3 \vphantom{#1#2} \right>} % for Dirac matrix elements
\newcommand{\grad}[1]{\gv{\nabla} #1} % for gradient
\let\divsymb=\div % rename builtin command \div to \divsymb
\renewcommand{\div}[1]{\gv{\nabla} \cdot #1} % for divergence
\newcommand{\curl}[1]{\gv{\nabla} \times #1} % for curl
\let\baraccent=\= % rename builtin command \= to \baraccent
\renewcommand{\=}[1]{\stackrel{#1}{=}} % for putting numbers above =
 \newcommand{\Ei}{\operatorname{Ei}}
\newcommand{\intd}{\operatorname{ \ \mathrm{d}}}
%Define and personal commands here

%Graphics Path to find your pictures
\graphicspath{{../Pictures/}}

\begin{document}

\chapter{Tools and methods}

The Atomic Force Microscope is an amazing instrument that has brought unprecedented insights into molecular structure and dynamics since its inception, and continuing into modern times (cite Visualizing complex mixtures of petroleum at atomic resolution / Monitoring tensile strength of all sorts of neat single-molecule polymers). The principle of an AFM is very similar to a record player. In an AFM, the substrate with DNA, protein, or sugar is the record disc; the cantilever is like the record stylus; the piezo is similar to the motor that spins the record. Both instruments measure the topography of the substrate (a record disc, or that which contains protein/DNA/sugar) using the deflection of the stylus/cantilever. The innovation in the AFM is that the cantilever is designed to have very few atoms on the tip to allow it to interact with only a single molecule or subset of atoms.

The components of the AFM has changed very little since its inception in 1986. All AFM instruments are composed of a scanner (with feedback), a sample area with substrate, a probe for monitoring the substrate, and a detector to monitor the probe. Each of these components have gone through many iterations of improvements which I will briefly describe.

\subsection{AFM probe design}

All AFMs are founded by a good probe - which is a cantilever with a very sharp tip - which provides the means in which the sample is interacted with by the device. The tip is microfabricated using photolithography so that the radius of curvature is on the order of nanometers. Though small, the cantilever itself generally follows Hooke's law

 \begin{equation}\label{eq:smfs-hookes} 
F = k_c x
 \end{equation}
 
where $k_c$ is the spring constant of the cantilever and $x$ is the displacement. A typical rectangular cantilever can be treated and solved using the beam equation \cite{butt2005force} to give the spring constant


 \begin{equation}\label{eq:smfs-rectangular} 
k_c = F / x = \frac{Ew t_c^3}{4 L^3}
 \end{equation}
 
where $L$ is the length, $E$ is the Young's modulus, $w$ is the width, and $t_c$ is the thickness of the cantilever. The resonance frequency for this type of the cantilever is then

 \begin{equation}\label{eq:smfs-rectangularres} 
f_{res} = 0.1615 \frac{t_c}{L^2}\sqrt{E/\rho}
 \end{equation}
 
where $\rho$ is the density of the cantilever material.

Ideal cantilevers have a high senstivity, which equates to a low spring constant, $k_c$. Looking at equation \ref{eq:smfs-rectangular}, it can easily be seen that this is accomplished by decreasing the thickness and increasing the length of the cantilever. Unfortunately, this is not the case. For almost all practical applications, there is an abundance of thermal noise due to small vibrations in the building or environment. Thus, it is optimal to seperate the resonant frequency of the cantilever from the low frequency of the noise in the environment. Thus, a truly optimal cantilever should have high sensitivty and a high resonant frequency.

Since the spring constant has equal contributions from the thickness and the length, and the resonant frequency has a square dependence only on the length (eq. \ref{eq:smfs-rectangularres}), it is practical to simply decrease the length of the cantilever in order to keep a consistently high resonant frequency while decreasing the thickness of the cantilever to provide a higher sensitivity.


\subsection{AFM detector design}

The detector is paramount to measuring the deflection of the probe which holds the information about the sample itself. The first incantation of the AFM used a STM for the probe and detector. This had the benefit of high precision in the displacement because it was using the electron tunneling for accurate measurement of the distance between the probe and the surface. However, this design was cumbersome and difficult because it required calibration and a vacuum in order to function effectively, and was not adequate for biological samples.

Another AFM detector design was to integrate electronics into the cantilever itself, so that the cantilever body harbored strain gauge sensors. This also, were difficult to use and often not as accurate as other methods as it measures the displacement over the body of the cantilever and not from the tip.

The most common method used for detection is the optical lever method. This method entails the use of a laser which reflects off the tip of the cantilever into a quadrant photodetector. The division in the photodetector allows quantifying how much of the laser light is diverted into the top/bottom halves which can be used to calculate the deviation from the mean position of the cantilever as it is displaced. As will be discussed later, this method requires a calibration of the photodiode voltage to quantify the displacement of the cantilever. The disadvantage of this method lies in the calibration, as it will often differ from the true values by 10\%.

\subsection{AFM calibration}

Though each cantilever carries manufacturer's rating of the spring constant, it is important that the AFM be calibrated before each experiment. Thermal fluctuations are considered as the only motion of the oscillator with the Hamiltonian
  
 \begin{equation}\label{eq:smfs-hamiltonian} 
H = \frac{p^2}{2m} + \frac{1}{2} k_c x^2.
 \end{equation}
 
According to the equipartition theorem, the average energy for a mode is given by 

 \begin{equation}\label{eq:smfs-equi} 
\left< \frac{1}{2} k_c x^2 \right> = \frac{1}{2} k_b T
 \end{equation}
 
where $k_b$ is the Boltzmann’s constant, $k_c$ is the spring constant of the oscillator, $T$ is the absolute temperature, and $x$ is the displacement of the oscillator. Therefore, $k_c$ can be obtained by measuring the mean-square spring displacement $\left<  x^2 \right>$ due to thermal fluctuations at room temperature. However, it is impractical to measure $\left<  x^2 \right>$ directly as it contains all the vibrational modes and also contains low-frequency thermal noise.

In order to capture only the flucuations from the first mode (away from the noise) the flucuations can be determined using Parseval's theorem which relates the square flucuations in time to the power of the frequency of the flucuations:

 \begin{equation}\label{eq:smfs-equi} 
\int_{-\infty}^{\infty}\left| x(t) \right|^2 dt = \int_{-\infty}^{\infty}\left| X(f) \right|^2 df.
 \end{equation}

Instead of taking the limits to infity, these integral can be computed by taking the area of the power spectrum for the first mode, which gives the power spectral density, $PSD$. However, the power spectral density is in terms of the photodiode observable - which is the square difference in volts between the upper and lower halfs of the photodiode quadrants. 


\begin{figure*}[h1]
\centering{\includegraphics[width=23pc]{../Figures/SMFS/Sensitivity.pdf}}
\caption{Experiment for measuring cantilever sensitivity.}
\label{fig:smfs-sensitivity}
\end{figure*}

To convert the power spectral density to a measurement of the square flucuations in the deflection, we can compute the cantilever sensitivity. This experiment is shown schematically in Figure \ref{fig:smfs-sensitivity}. 
The cantilever sensitivty, $\sigma$ is determined by pressing the cantilever against the surface a known distance (which is measured using a strain-gauge sensor in the piezo itself), while simultaneously measuring the change in the voltage

 \begin{equation}\label{eq:smfs-sens} 
\sigma = \Delta V / \Delta z
 \end{equation}

Thus, the mean square cantilever deflections, $\left< \frac{1}{2} k_c x^2 \right>$ can be given by the power spectral density over the square sensitivity, $PSD / \sigma^2$ which gives the final formula for calculating the spring constant, $k_afm$, from the equipartition theorem:

 \begin{equation}\label{eq:smfs-sens} 
k_{afm} = \frac{k_b T}{PSD}\sigma^2.
 \end{equation}






\bibliographystyle{unsrt}	
\bibliography{../Bibliography/References} %your bibliography file - change the path if needed	
\end{document}
