
import os
dafiles = []
for root, dirs, files in os.walk("./"):
    for file in files:
        if file.endswith(".tex"):
             dafiles.append(os.path.join(root, file))

for dafile in dafiles:
	doc = open(dafile,'r').read()
	badcitestring = '. \cite{'
	start = doc.find(badcitestring)
	while start != -1:
		end = doc[start:].find('}')+start+1
		citation = doc[start+2:end].strip()
		puncuation = doc[start].strip()
		original = doc[start:end]
		replacement = " " + citation + puncuation
		doc = doc.replace(original,replacement)
		start = doc.find(badcitestring)
		print(original + " -> " + replacement)

	badcitestring = '.  \cite{'
	start = doc.find(badcitestring)
	while start != -1:
		end = doc[start:].find('}')+start+1
		citation = doc[start+3:end].strip()
		puncuation = doc[start].strip()
		original = doc[start:end]
		replacement = " " + citation + puncuation
		doc = doc.replace(original,replacement)
		start = doc.find(badcitestring)
		print(original + " -> " + replacement)

	badcitestring = '.\cite{'
	start = doc.find(badcitestring)
	while start != -1:
		end = doc[start:].find('}')+start+1
		citation = doc[start+1:end].strip()
		puncuation = doc[start].strip()
		original = doc[start:end]
		replacement = " " + citation + puncuation
		doc = doc.replace(original,replacement)
		start = doc.find(badcitestring)
		print(original + " -> " + replacement)

	badcitestring = ',\cite{'
	start = doc.find(badcitestring)
	while start != -1:
		end = doc[start:].find('}')+start+1
		citation = doc[start+1:end].strip()
		puncuation = doc[start].strip()
		original = doc[start:end]
		replacement = " " + citation + puncuation
		doc = doc.replace(original,replacement)
		start = doc.find(badcitestring)
		print(original + " -> " + replacement)

	with open(dafile,'w') as f:
		f.write(doc)