Force-spectroscopy relies upon the tethering of the molecule of interest to a surface in order to be able to forcefully unfold the molecule. This tethering is often done ``non-specifically'', that is the protein is not specifically modified to attach to the surface. Here, we attempt to specifically attach the protein using a very simple chemistry - thiol linkages provided between cysteines and a gold surface.

Using the automation procedure we were able to obtain statistics about the frequency of force extension curves.  As an example of the power of such statistics, we quantified a common experimental practice of using single thiol group at the N-terminus and Au-covered cantilever or substrate to record force-extension traces with higher efficiency (more usable unfolding events) or with higher efficacy (more full length molecules unfolded).  This is an important aspect of force spectroscopy, because there is a range of the number of domains that will be present during unfolding (Figure \ref{fig:smfs-S11}), although the maximum number is always optimal.



\begin{figure}[h!]
	\centering{\includegraphics[width=35pc]{../Figures/SMFS/S11.pdf}}
	\caption[Examples of unfolding I27 domains 1-8]{Representation of the recordings used to count the number of modules unfolded.  Heuristics for selection (details in Materials and Methods) designate that unfolded modules in the force-extension recording (black line) must fit to a worm like chain (WLC) family (red dashed line) with contour length increment spacing of 28nm and persistence length of 0.35nm and the peaks must be within acceptable force limits for unfolding of I27.}
	\label{fig:smfs-S11}
\end{figure}

The application of the bond that forms between a thiol group of the Cysteine residue and Au has been an important advance for biomolecular assembly \cite{Nuzzo1983}. It has been exploited for specific adhesion of a molecule to a substrate in protein biology \cite{Nakanishi2008}, and biological membranes \cite{Mrksich1996}.  It has been shown that the specific adhesion of a molecule to a cantilever tip can be attained through functional modification of the cantilever tip \cite{Zhang2009a,Schmitt2000a,Kamruzzahan2006}.  However it is unknown whether the specific adhesion between the protein and the tip will increase the probability of picking up full length molecules.  

To test experimental conditions for optimizing the number of full length recordings, we performed AFM measurements on six systems using the Cys-I277 protein construct and the I277 protein construct: 1) I277 with MLCT on glass, 2) I277 with OBL on glass, 3) Cys-I277 with MLCT on glass, 4) Cys-I277 with OBL on glass, 5) I277 with MLCT on gold, or 6) Cys-I277 with MLCT on gold.  These systems were measured over several prepared samples, with an average of 6,630 recordings per sample for an average total of 60,749 recordings for each system (~8 experiments for each).  Each system was used with old (>3 month) and new (< 2 week old) proteins.


\begin{figure}[h!]
	\centering{\includegraphics[width=35pc]{../Figures/SMFS/S1.pdf}}
	\caption[Sequencing of pRSET-A plasmid to introduce Cysteine]{The original vector, based on the pRSET-A vector, was mutated using PCR extension.  The forward primer contained base pairs 114 to 144 of our pRSET-A based vector (5’ ATCATCATCATCATCATTGTCTGGTTCCGCG 3’) carrying the Gly->Cys mutation in the 131st base pair.  The reverse primer contained the complemented base pairs 78 to 113 (5’ GAGAACCCCGCATATGTATATCTCCTTCTTAAAGT 3’).  These primers were phosphorylated on the 5’ end to initiate ligation after PCR.  The PCR reaction was carried out using PfuUltra II Fusion HotStart DNA polymerase (Agilent Technologies, Santa Clara, CA) according to their protocol.  The PCR product was directly ligated and transformed into Turbo competent E. coli (New England BioLabs, Ipswich, MA).  The cells were lysed and DNA precipitated and sequenced to verify the desired mutation.}
	\label{fig:smfs-S1}
\end{figure}


For this experiment we used a polyprotein composed of seven I27 domains connected with short two amino acid linkers which we refer to as I277.  The gene for I277 was obtained from modifying the 8th terminal of the poly(I27) pRSETa vector, a kind gift from Jane Clarke \cite{Steward2002}, engineered to have a C-terminal 6xHis-tag. The Cys-I277 construct was made by mutating the Gly residue after the N-terminal 6xHis-tag into a Cys, which should leave an unburied thiol group for reaction with the Au tip or surface.  The mutation was made using 5’ phosphorylated primers with the desired mutation and verified by sequencing (Figure \ref{fig:smfs-S1}). All engineered plasmids were transformed into Escherichia coli C41(DE3)pLysS cells, and expression was induced using isopropyl  -D-thiogalactopyranoside and purified on a Ni-NTA column (Qiagen, Valencia, CA) and stored in elution buffer (PBS pH 7.4 with 250 mM Imidazole) at 4C.



In addition to the engineered cysteines in the front of the plasmid, each I27 molecule could potentially contribute two thiol bonds from cysteines, Cys63 and Cys47.  However, the crystal structure of I27 \cite{Stacklies2009} shows that only Cys47 is exposed to the solvent but that the sulfur atom is partially buried in a pocket made by the loop region between stand C and strand D (Supplementary Figure S2).  To test whether any of these cysteines are reactive, we used attachment to 5’ thiol modified DNA handles as an assay.  We followed the protocol for attaching 5’ thiol modified DNA handles for the I277 protein and the Cys-I277 protein (Supporting material) and visualized using AFM imaging \cite{Cecconi2011}. The number of full length DNA handles with zero, one, or more than one protein attached was determined for each set of images.  Whole DNA handles were determined based on their length (0.35 nm = 1 bp) and the presence of a protein attachment was determined by a 1-3 nm bump on the end of the DNA molecule.  DNA handles attached to a Cys-I277 protein in 10\% of the handles while the other 90\% of the DNA handles attached to no Cys-I277 proteins (n=281, Supplementary Figure S4A). For the unmodified I277 protein we found that 0\% of the DNA handles attached to a protein (n=302) (Supplementary Figure S4B). This data indicates that only the Cys-I277 protein construct is able to form a single thiol bond while the unmodified I277 protein construct is incapable of forming thiol bonds. Therefore we can test whether the thiol bond capable Cys-I277 protein construct can produce more full length recordings due to the attachment to the gold-covered substrate or a gold covered AFM tip.



\begin{figure}[h]
	\centering{\includegraphics[width=35pc]{../Figures/SMFS/S2.pdf}}
	\caption[Structural evidence of exposed cysteine in I27]{ Investigation of free thiols on native I27 using crystal structure 1WAA.  I27 normally has two Cysteine residues (Cys47 and Cys63).  Using a probe radius of 0.14 nm shows that the protein surface exposes only Cys47 slightly to the solvent.  The sulfur atom of Cys47 is not likely reactive to a surface because it is buried in a pocket made by the loop between strand C and strand D of the I27.}
	\label{fig:smfs-S2}
\end{figure}

To test the number of free thiol bonds in the I27 construct we developed an assay using DNA handles instead of 2,2’-dithiodipyridine (DTDP).  The size exclusion properties of DNA handles should be more realistic than using the small molecule DTDP. We generated DNA handles of two sizes using PCR from the pRSET-A plasmid.  The primers were synthesized by Integrated DNA Technologies®.  The forward primer was the same in each case, 5’ GAAGCTTGATCCGGCTGC 3’ with a 5’ Thiol modifier C6 S-S for attachment to the free thiols.  A 916bp fragment was made using the reverse primer 5’ CAGGAAGGCAAAATGCCGC 3’ and a 318bp fragment was made using the reverse primer 5’ GAAGAAAGCGAAAGGAGCGG 3’.  The PCR was verified by gel electrophoresis (Figure S6). The 916bp and 318bp DNA handles were reacted with the Cys7I27 and the 7I27 respectively according to the Cecconi et al. protocol.   

\begin{figure}[h]
	\centering{\includegraphics[width=35pc]{../Figures/SMFS/S4.pdf}}
	\caption[AFM imaging to test reactivity of buried I27 cysteines]{ Representative AFM images of proteins reacted with 5’ Thiol modifier C6 S-S DNA handles to test for thiol accessibility.  Red arrows indicate full length DNA handles, green arrows indicate proteins and purple arrows indicate DNA handle reacted with protein. (A) I277 with 316bp DNA handles. (B) AFM image of Cys-I277 with 918bp handles.  We found that 0\% (n=302) of the DNA handles reacted any amount I277 protein, while 10\% (n=281) of DNA handles reacted with a single Cys-I277 protein and none more.  This data indicates that even though each I27 has two Cysteines, they are not accessible to size excluded volumes like DNA handles and should not react with surfaces or tips of the cantilever.}
	\label{fig:smfs-S4}
\end{figure}

Freshly cleaved mica slides were incubated with 1-(3-Aminopropyl)silatrane (APS) according to the protocol by Shlyakhtenko et al. for AFM imaging. The DNA handle reaction with protein was incubated on APS-mica for 3 minutes and then rinsed and dried with air.  AFM images were taken with a Nanoscope IIIa MultiMode Scanning Prove Microscope (Veeco Instruments Inc.) using Tapping mode in air with RTESP proves (spring constant 20-80N/m and resonance frequency 275-315kHz).  Images were taken at a resolution of 512x512 pixels with a scan size of 0.5 – 2 µm. Representative images are shown in Figure \ref{fig:smfs-S4} and show that the cysteines within the I27 domains are not reactive. 


\subsection{Terminal Cysteine improves efficiency for tethering}



An AFM system with high efficacy would have a high proportion of single molecule recordings that unfold the full length molecule.  Such a system will have a better chance of unfolding the protein of interest and would be very useful for researchers’ data collection. Efficacy results for the six systems are shown in Figure \ref{fig:shuffled3}.  The distributions for Cys-I277 OBL-Glass, I277 MLCT-Gold and Cys-I277 MLCT-Gold had a great proportion of molecules with more unfolding events than either I277 MLCT-Glass, I277 OBL-Glass, or Cys-I277 MLCT-Glass (p < 10-5 with Bonferroni correction for all tests).  The Cys-I277 MLCT-Gold shows higher proportion of full length molecules than I277 MLCT-Gold, however this observation is not statistically significant (p = 0.14 with Bonferroni correction).

It is possible that the Cys-I277 could react with itself to form an I277-Cys-Cys-I277  protein. This would result in a higher number of longer molecules when using an unreactive substrate and tip as compared to using I277 with the same experimental conditions.  Comparing the distributions for Cys-I277 MLCT-Glass and I277 MLCT-Glass we found that the null hypothesis that the distributions come from the same underlying distribution could not be rejected (p=0.73).  Thus the intra-molecular connections formed between Cys-I277 did not influence the results.



Overall, these results are consistent with our hypothesis that an unburied thiol group at the end of the molecule would increase the proportion of longer molecules picked up by an Au-covered tip or on a gold coated surface.  Surprisingly, the I277 MLCT-Gold system was also highly efficacious.  This could be due to the inert reactivity of gold.  Glass may be stickier and better able to hold down the protein than gold.  However, combining the Cys-I277 protein with the Gold substrate and MLCT cantilever seemed to have the best overall results, combining the assets of the gold substrate and the connection between the free thiol and the gold surface.


\begin{figure}[tbp]
\begin{center}
\includegraphics[width=4in]{ConditionOptimization}
\end{center}
\caption[Optimal experimental conditions for full length proteins]{The probability for detecting 1-7 force peaks is plotted, normalized against the total number of molecules in which force peaks were detected.  }
\label{fig:ConditionOptimization}
\end{figure}

An AFM system with high efficiency is able to pick up more single molecule recordings, though not necessarily full length recordings.  Efficiency for the six systems was determined by counting the average number of positive recordings that were obtained during a single experiment.  A single experiment consisted of a freshly prepared sample on newly cleaned glass and an average of 6,630 total force-extension recordings.  Since all samples were diluted to the same concentration and placed on the same substrate we expect that the density of molecules between each experiment should be equal.  
We compiled the results from different tests on the six different experimental setups and did ANOVA assuming that components contributing to the variation were either protein type (I277 or Cys-I277), substrate type (gold or glass), cantilever type (OBL or MLCT), protein age (old or new), or their interactions. The only statistically significant source of variation was the protein age (p=0.008 with Bonferroni correction) shown in Figure

