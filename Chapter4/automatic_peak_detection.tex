Though this field is promising, SMFS is hindered by the slow and tedious data collection which is necessary in AFM experiments to ensure accurate and reproducible data.  Even with a positive control, the recording of a single molecule with the protein of interest is a relatively rare event; often at least 50,000 recordings are needed before enough unfolding events are selected to create a converged histogram of the characteristic contour length increment and unfolding forces.  Normally the researcher saves only a few hundred recordings during data acquisition in order to save space and analysis time.  Dynamic force spectroscopy \cite{Evans1999,Merkel1999,StrunzOroszlanSchaferEtAl1999} experiments must be conducted at several pulling speeds which increases the required amount of data by a factor of four.  Each recording is done at a speed of 5-5000 nm/s so a given recording may take 0.2-30 seconds; this means that the full mechanical characterization of a protein can require 12-1400 hours of operating the AFM (not including gene engineering, protein purification, and sample preparation).  

%Automation can be accomplished using an algorithm that triages usable force-extension recordings quickly with positive and negative selection. I implemented an algorithm based on the windowed fast Fourier transform of force-extension traces that identifies peaks using force-extension regimes to correctly identify usable recordings from proteins composed of repeated domains.  This algorithm excels as a real-time diagnostic because it involves <30ms computational time, has high sensitivity and specificity, and efficiently detects weak unfolding events. We used the statistics provided by the automated procedure to clearly demonstrate the properties of molecular adhesion and how these properties change with differences in the cantilever tip and protein functional groups and protein age.  

Automation has become more recurrent in science fields, enabling better reproducibility and allowing higher throughput of scientific data \cite[]{King2009,Muggleton2006}. A notable example is the robot developed by Oliver et al. that generates hypotheses and performs functional genomic assays which are simple - but laborious – experiments \cite[]{King2004}. Many drawbacks of AFM can be partially removed using an accurate automated procedure for capturing and selecting data.  Currently, most AFM operation is done by trained researchers except for a few recent advances that allow for the automation of calibration of cantilever spring constants \cite[]{Materassi2009} and saving most recordings using a force threshold \cite[]{Jens2008}. This last method works well for many proteins. However, proteins with weak unfolding events (less than 15 pN) would require setting the threshold near the baseline which would capture nearly all empty or nonspecific recordings. Since only 1\% of data is usable it is not advisable to save the other 99\%, which would require additional storage and backup resources and make future reviewing cumbersome. 

How, then, can one save only the usable force-extension recordings from AFM experiments in real-time? Accurate analysis of force curves can be performed through algorithms that use methods like force thresholds \cite[]{Jens2008}, fuzzy logic \cite[]{Kasas2000}, peak detection \cite[]{Odorico2007}, transformation into contour length space \cite[]{Puchner2008b}, pattern recognition \cite[]{Kuhn2005}, convolution functions \cite[]{Sandal2009} or correlation functions \cite[]{Dietz2007} or combinations of these methods \cite[]{Andreopoulos2011,Bosshart2012}.  A given AFM experiment may yield only 0.5-1\% of usable recordings, 20-30\% nonspecific recordings, while the remainder are often blank recordings.  Thus, a successful real-time algorithm should be able to accept only the small fraction of the recordings that are usable (positive selection) and efficiently reject all other recordings (negative selection) to save space and time.

With these characteristics in mind, I developed an algorithm for accurate automation of real-time data collection for AFM stretching experiments of polyproteins. This automation helped to increase the throughput of our laboratory because it allows parallel experimentation and increased duration of experiments. Also, this procedure instantly provides new information about the statistics of usable and nonspecific recordings which can be exploited to describe surface and tip chemistry effects on molecules for AFM. This work serves as an important step toward creating a fully-automatic AFM system that operates similarly to other spectroscopy machines.  


\subsection{Generating test set of curves}

To gauge efficiency of different peak detection algorithms, I need a systematic set of data that accurately represents force recordings. For this reason I decided to generate test force curves with similar characteristics to real force recordings. This has the benefit in that I can choose the level of noise, the magnitude of the force rupture, the distance between rupture events, and the presence of nonspecific adhesion.

\begin{figure}[h!]
	\centering{\includegraphics[width=20pc]{../Figures/Automation2/TestsetGeneration.pdf}}
	\caption[Generating force-curve set for testing]{Method for generating a test-set of force-extension curves for testing algorithms for automatic peak detection.}
	\label{fig:smfs-test-set-generation}
\end{figure}


Test curves were generated as shown in Fig. \ref{fig:smfs-test-set-generation}. First, a force and contour length increment was selected.  Then the worm-like chain was used to generate multiple peaks where the peak force was determined from a normal distribution with a mean set from the initial parameters. A white-noise generator was used to then add RMS noise to the curve at a specified threshold. After white-noise, Brownian noise was added to simulate experimental drift with a modest diffusion coefficient. For subsequent tests, at least 100 test curves were generated for any given tuple of force rupture level, contour length increment, or noise level.




\subsection{Automatic baseline correction}

The force-spectroscopy data that is of highest value are the relative values between points (between extension or between forces). However, to accurately determine fits to worm-like chains or other contour-length models, the force-extension curve must be normalized so that the curve starts at the origin (zero extension and zero force). Since the raw forces and extension are arbitrary (they are converted from raw voltages in the photodiode or strain-gauge sensor), they must be shifted in order to put the recording on the correct axis.


\begin{figure}[h!]
	\centering{\includegraphics[width=35pc]{../Figures/SMFS/RecordingBaseline.pdf}}
	\caption[Automation of baseline detection]{An example of the procedure to automatically detect the baseline and point of origin in the force-extension profile.}
	\label{fig:smfs-baseline}
\end{figure}


To determine the baseline correct, I determine the point of intersection between the baseline (where the force should be 0) and the beginning of the curve (where the extension should be 0). First, the I performed least-square fitting with perpendicular offsets to the beginning of the recording to get a vertical or near vertical line that represents the extension. Then I fit the last 100 points using regular linear regression. The intersection of these two lines allows the origin of the force-extension recording to be determined, as shown in Figure \ref{fig:smfs-baseline}. The entire recording is thus shifted horizontally and vertically so that this intersection point will become the origin (zero force and zero extension).



\subsection{Windowed fast-Fourier transform for peak detection (WFFT method)}
\label{section:wfft}

The first method I investigated for peak detection is the windowed fast-Fourier transform method (WFFT). This method uses two steps, 1) using a windowed fast Fourier transform across the recordings, and then 2) using preset force-extension regimes to detect usable peaks and assess whether the recording is usable. This method is efficient at capturing force peaks because of the discontinuity that occurs when the force drops during an unfolding event. The force drop creates a discontinuity in the signal causing the Fourier sums to overshoot the original function (known as the \"Gibbs phenomenon\"). The overshooting of the Fourier sums at discontinuities provides a signal containing peaks at each unfolding event, and allows for accurate identification of unfolding events, even in the presence of noise.

The algorithm to produce a windowed fast Fourier transform of the data is as follows (scripts and test sets of proteins are available at \url{http://smfs.pratt.duke.edu/downloads.html}):
\begin{enumerate}
	\item Designate a window size that is smaller than the smallest unfolding contour length increment.  For this study a window size of 5 nm was used.
	\item Allocate an empty matrix to hold the FFT coefficients for each point of the windowed recordings.  The number of rows should be equal to the number of windows to take the FFT and the number of columns should be equal to the half of the window length.
	\item Set location to the beginning of the FE vector.
	\item 	Draw a window at the current location of the FE vector.
	\item 	Calculate the magnitude of the FFT of the windowed region at the current location.  The windowed region need not be a power-of-two as the small window size can be efficiently calculated using a FFT algorithm that takes advantage of small prime factors.
	\item Save the magnitude of the FFT coefficients to a new row in the matrix.
	\item 	Step to next point to take the next window and go back to step (d) until the end of the FE vector is reached.
	\item 	The WFFT is calculated from the sum of the odd coefficients, by adding the odd columns of the matrix.  The corresponding sum is the vector containing the WFFT.
\end{enumerate}

The determination of the force-extension peaks is straightforward.  First, all peaks are detected by comparing each element of the WFFT to its neighboring values. A local maximum is declared if it is larger than both its neighbors.  The peaks are then sorted in descending order according to their heights. Starting from the highest peak, any subsequent peak separated by a distance smaller than the minimum FE regime extension increment (Figure \ref{fig:WFFT}B) is ignored. Additionally, any peaks less than the minimum force specified in the FE regime are ignored. To determine the type of unfolding event for each local maximum, the user sets a 2D space for the possible forces and extensions of a given molecule (Figure \ref{fig:WFFT}B).  The force of peaks and distances between peaks from the WFFT of the FE recording are analyzed and then each peak is assigned specific types or declared nonspecific if no type is found.

\begin{figure}[h!]
	\begin{center}
		\includegraphics[width=4in]{WFFT}
	\end{center}
	\caption[Example of WFFT procedure]{Example of procedure correctly identifying the unfolding events in a recording of I273-NI10C-I273 (NI10C)  which contains both small, tightly spaced Ankyrin unfolding events, and high-force I27 unfolding events.  (A) The windowed fast Fourier transform (WFFT) of the force-extension (FE) curve is calculated as described in 2.1.1.  The local maxima (red triangles) are then determined by comparing each element of the WFFT with its neighbors. (B) The user supplies the force-extension regimes for necessary protein unfolding events.  (C) The peaks are assigned to be specific proteins based on distance between neighboring peaks and their force threshold according to the force-regimes (here only regimes for Ank and I27 are used). }
	\label{fig:WFFT}
\end{figure}


\subsubsection{Fidelity for different FE curves}


To measure performance, I generated test curves at difference mean unfolding forces and measured the true-positive rate (TPR) of selection and the false-positive rate (FPR) of selection. The TPR quantifies the percentage of actual peaks (force level and extension) that were correctly identified in the data, while the FPR quantifies the percentage of peaks that were introduced during the analysis that are not actual peaks in the data. These results are shown in Figure \ref{fig:smfs-wfft-figure}. 

\begin{figure}[h!]
	\centering{\includegraphics[width=35pc]{../Figures/Automation2/WFFT.png}}
	\caption[WFFT method for peak determination TPR and FPR]{True positive rate (left) and False positive rate (right) for test recordings of varying contour-length increment (y-axis) and varying unfolding force (x-axis).}
	\label{fig:smfs-wfft-figure}
\end{figure}

This shows that the WFFT is able to have near 100\% accurate detection with 0\% false positives at forces above 40 pN and contour-length increments above 20 nm. The peaks that are too small are unable to be captured by this method, as well as peaks that are too close together. This is likely the case because the window size must be small enough to encompass only a single rupture event, and the window size cannot be decreased beyond a certain point as it will not have enough data to be able to perform the transform. 


\subsubsection{Effect of noise on the fidelity of algorithm}

Noise is inherent in AFM experiments and can be alleviated using lower loading rates and decreasing recording bandwidths for unfolding, or by sophisticated techniques like Lock-in amplifiers \cite{Anczykowski1999}. Here we used a test set of proteins with various levels of synthetic noise (as determined by RMS) added to the original signal.  An example is shown in Figure \ref{fig:FEDetectionWithNoise} and the results are tabulated in Table 2.  The baseline RMS noise in all recordings is between 3 pN and 10 pN.
The FE of I27 proteins are resistant to large amounts of noise because of their high unfolding force and large extension increment.  The sensitivity is unchanged upon adding up to 12 pN of noise and the detection of recordings with only three I27 peaks becomes less sensitive upon adding 24 pN.  The I27 force regime is also very specific, as adding up to 24 pN had no effect on the negative selection.
The FE of NI6C and NI10C proteins are also well defined in their force regimes so that they remain perfectly sensitive for positive selection up to 24 pN.  However, adding more noise increases the chance of small nonspecific peaks that resemble the Ank FE regime and the specificity decreases drastically between adding 6 pN and 24 pN of noise.  The FE of SNase is also well defined, although there is a slight drop in sensitivity when adding 12 pN of noise.  There is also a drastic drop in sensitivity for SNase when increasing the noise level from 12 pN to 24 pN, because the added noise is approaching the FE regime of SNase. As a result, more nonspecific events become present.

\begin{figure}[tbp]
	\begin{center}
		\includegraphics[width=4in]{FEDetectionWithNoise}
	\end{center}
	\caption[Example of WFFT with added noise]{Representative example of adding noise to a recording of NI10C.  The detected peaks are shown with triangles and then amount of noise added is labeled (RMS of the noise).  The detection of the small Ank unfolding events become less reliable as the noise is increased, but remain fairly stable until 12 pN of noise is added.  The identification of I27 events is very reliable up to 24 pN, although many nonspecific events become present at this high level of noise.}
	\label{fig:FEDetectionWithNoise}
\end{figure}

\subsection{Segmentation filtering for peak detection (SegM method)}

The \texttt{WFFT} method is useful and viable for many scenarios, it seems to fail at lower contour-length increments and rupture forces. I sought to improve the automatic peak detection through a novel method - segmentation filtering (SegM). In this method, a raw force-extension curve is filtered by taking the segments from a segmentation method (like a linear Kalman filter) and using those as the proxies for the rupture points. This method is fast and reliable based on a generated test set of recordings.


\begin{figure}[h]
\centering{\includegraphics[width=35pc]{../Figures/SMFS/LinearInterpolation.pdf}}
\caption[Example of linear interpolation in segmentation]{Example of the SegM method. First the original data (blue) is segmented using a Kalman-filter (red horizontal lines). The center of each segment is used for tracing (pink) and the traced line is used to determined the peaks.}
\label{fig:smfs-LinearInterpolation}
\end{figure}

Peak detection was accomplished using a simple procedure. First, the raw force curve data was divided into segments of constant levels using an algorithm of the Kalman filter type (\texttt{segm()} in Matlab). The specific model used was $y(t) = b_1 u (t-1)$ where $b_1$ is the model parameter determining the piecewise constant level of the estimated output, $y(t)$. The midpoint of each segment was then used and a new curve was built using linear interpolations between the captured midpoints. An example of this procedure is shown in Figure \ref{fig:smfs-LinearInterpolation}. This method is very successful in removing noise and allowing peak detection as shown in Figure \ref{fig:smfs-PeakDetection}.

\begin{figure}[h]
\centering{\includegraphics[width=35pc]{../Figures/SMFS/PeakDetection.pdf}}
\caption[Example of SEGM peak detection TRP and FPR]{Example of the SegM method where the force-extension trace with typical noise (top) is filtered using the SegM method (bottom) and the peaks are determined (red dots).}
\label{fig:smfs-PeakDetection}
\end{figure}

Quantitative results are quite revealing. To measure performance, I again generated test curves at difference mean unfolding forces and measured the true-positive rate (TPR) of selection and the false-positive rate (FPR) of selection. The TPR quantifies the percentage of actual peaks (force level and extension) that were correctly identified in the data, while the FPR quantifies the percentage of peaks that were introduced during the analysis that are not actual peaks in the data. These results are shown in Figure \ref{fig:smfs-SegmResults}. In general, the SegM method is good at determining peaks for any contour-length increment, as long as forces are higher than $\sim$ 20 pN. This is a great improvement upon the WFFT described earlier in this chapter which saw very fidelity at small contour-length increments. However, the false-positive rate here is slightly here at any peak-type, leveling around 10-20\%. This may be because the high amount of filtering on the raw data may create artificial peaks near the baseline, that are getting adequately removed. 


\begin{figure}[h!]
	\centering{\includegraphics[width=35pc]{../Figures/Automation2/SEGM.png}}
	\caption[SEGM method for peak determination TPR and FPR]{True positive rate (left) and False positive rate (right) for test recordings of varying contour-length increment (y-axis) and varying unfolding force (x-axis).}
\label{fig:smfs-SegmResults}
\end{figure}



\subsection{Contour-length transformation for peak detection (Lc method)}


Though both the \texttt{WFFT} and \texttt{SegM} had useful characteristics, there was still yet another method that could be developed for particular scenarios (or combined with the previous methods for additional confidence). This method, denoted the \texttt{Lc} method, simply tries to find a force-peak in between bounds generated by a contour-length histogram. This has the benefit of relying heavily on the raw force-extension data and does not require filtering.

\begin{figure}[h!]
	\centering{\includegraphics[width=35pc]{../Figures/Automation2/LC_method-3-01.png}}
	\caption[LC schematic for peak determination]{Example of Lc method for determining a peak. Top shows a force-extension trace where the red lines are 95\% bounds on the Gaussian peak that comes from the force-contour trace (bottom). The peak determined is the maximum between the red line bounds.}
	\label{fig:smfs-example-lc}
\end{figure}

The method for peak detection using the force-contour plot (Lc method) is as follows. A force-extension curve (Figure \ref{fig:smfs-example-lc}, top) is transformed to a contour-length histogram by computing the contour-length at each point and plotting the relative frequencies. As seen in the bottom of Figure \ref{fig:smfs-example-lc}, their are several regions of high density due to the curves carrying the same contour-length (though different forces) for all the points leading up to the rupture event. Each of these distributions in the contour-length histogram can be determined automatically used a Gaussian mixture model. The 95\% bounds of a single distribution can then be determined, which provides two set points for contour-lengths. These contour-lengths can be transformed back to the force-extension profile (red lines in Figure \ref{fig:smfs-example-lc}) and the rupture point is then found by finding the maximal point within these bounds. Additional examples are shown in Figure \ref{fig:smfs-example-lc2}.



\begin{figure}[h!]
	\centering{\includegraphics[width=35pc]{../Figures/Automation2/LC_method3-01.png}}
	\caption[Examples of LC method for peak determination]{Further examples for determining the rupture peak (red dot) for force-extension traces (black, top) from the force-contour traces (blue, bottom).}
	\label{fig:smfs-example-lc2}
\end{figure}


As before, performances is evaluated using a test-set of curves that can be used to quantify the true-positive rate (TPR) of selection and the false-positive rate (FPR) of selection. The TPR quantifies the percentage of actual peaks (force level and extension) that were correctly identified in the data, while the FPR quantifies the percentage of peaks that were introduced during the analysis that are not actual peaks in the data. These results are shown in Figure \ref{fig:smfs-lcmethod}. In general, the SegM method is quite sensitive at determining peaks for any contour-length increment, and any unfolding forces (Figure \ref{fig:smfs-lcmethod}, left). However, this comes at the cost of a high false-positive rate, as shown in Figure \ref{fig:smfs-lcmethod} (right). Though it could be further optimized, the Lc method has the additional disadvantage in that it would perform worse if the force-extension profile of a molecule does not follow the model for determining the contour-length.


\begin{figure}[h!]
	\centering{\includegraphics[width=35pc]{../Figures/Automation2/LC.png}}
	\caption[LC method for peak determination TPR and FPR]{True positive rate (left) and False positive rate (right) for test recordings of varying contour-length increment (y-axis) and varying unfolding force (x-axis).}
	\label{fig:smfs-lcmethod}
\end{figure}



\subsection{Conclusion}


We show that there are feasible heuristics and software capabilities to automate the SMFS experimentation for stretching experiments of polyproteins by correctly identifying usable recordings from nonspecific recordings. This step is useful for AFM operators who spend tens to hundreds of hours manually operating AFM machines. Unfortunately, the simple method of saving all recordings is not feasible here because of the huge amounts of hard drive space necessary for data storage and preservation. Previous methods for AFM force-extension analysis prioritize accurately identifying each individual peak but this is too computationally intensive to be used in a real-time setting.  Thus, we developed an algorithm that performs selection of the usable recordings from the unusable nonspecific recordings in a timely manner that is efficient for real-time selection, which is applicable to weak and strong unfolding events in proteins composed of repeated domains. 

Automation is a simple procedure and provides an objective framework to the subjective process of data collection and selection from force-extension recordings.  We hope researchers are encouraged to automate their systems to expedite the production of high quality single molecule force spectroscopy data because there is still a great deal of molecular information that can be probed with this powerful instrumentation.  We hope to further develop these automation techniques to adapt it to other forms of single molecule measurements like AFM-FRET \cite{He2012} or AFM-TIRF \cite{Sarkar2004}. This development is the first step on the way to creating a fully automatic AFM-based force spectrometer that would operate similarly to traditional spectrometers such as CD, UV, VIS, IR or NMR in that it would generate and save useful force spectra with minimal User supervision.
